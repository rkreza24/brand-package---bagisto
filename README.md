### 3. Installation:

* Goto config/app.php file and add following line under 'providers'

~~~
Itec\Brand\Providers\BrandServiceProvider::class
~~~

* Goto config/concord.php file and add following line under 'modules'

~~~
\Itec\Brand\Providers\ModuleServiceProvider::class,
~~~

* Goto composer.json file and add following line under 'psr-4'

~~~
"Itec\\Brand\\": "packages/Itec/Brand/src"
~~~

* Run these commands below to complete the setup

~~~
composer dump-autoload
~~~

~~~
php artisan migrate
~~~

~~~
php artisan route:cache
~~~

~~~
php artisan vendor:publish

-> Press 0 and then press enter to publish all assets and configurations.
~~~


> That's it, now just execute the project on your specified domain.