<?php

namespace Itec\brand\DataGrids;

use Webkul\Ui\DataGrid\DataGrid;
use DB;

/**
 * BrandDataGrid Class
 *
 * @author    Md Rezaul Karim <rkreza24@gmail.com>
 * @copyright 2019 Itec Digital Ltd (http://www.itecdigitalbd.com)
 */
class BrandDataGrid  extends DataGrid
{
    protected $sortOrder = 'desc'; //asc or desc
    protected $index = 'id';
    protected $itemsPerPage = 20;

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('brands')
            ->select('id')
            ->addSelect('id', 'name', 'image', 'description','slug', 'status');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index'         => 'id',
            'label'         => trans('brand_lang::app.datagrid.id'),
            'type'          => 'number',
            'searchable'    => false,
            'sortable'      => true,
            'filterable'    => true
        ]);

        $this->addColumn([
            'index'         => 'name',
            'label'         => trans('brand_lang::app.datagrid.name'),
            'type'          => 'string',
            'searchable'    => true,
            'sortable'      => true,
            'filterable'    => true
        ]);

        $this->addColumn([
            'index'         => 'slug',
            'label'         => trans('brand_lang::app.datagrid.slug'),
            'type'          => 'string',
            'searchable'    => true,
            'sortable'      => true,
            'filterable'    => true
        ]);

        $this->addColumn([
            'index'         => 'status',
            'label'         => trans('brand_lang::app.datagrid.status'),
            'type'          => 'boolean',
            'sortable'      => true,
            'searchable'    => false,
            'filterable'    => true,
            'wrapper'       => function($value) {
                if ($value->status == 1)
                    return 'Active';
                else
                    return 'Inactive';
            }
        ]);
    }

    public function prepareActions() {
        $this->addAction([
            'type'          => 'Edit',
            'method'        => 'GET', // use GET request only for redirect purposes
            'route'         => 'brand.edit',
            'icon'          => 'icon pencil-lg-icon'
        ]);

        $this->addAction([
            'type'          => 'Delete',
            'method'        => 'POST', // use GET request only for redirect purposes
            'route'         => 'brand.delete',
            'icon'          => 'icon trash-icon'
        ]);

        $this->enableAction = true;
    }


}