<?php

namespace Itec\Brand\Repositories;

use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Event;
use Webkul\Core\Eloquent\Repository;
use Itec\Brand\Models\Brand;
// use Itec\Brand\Models\BrandTranslation;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Brand Reposotory
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class BrandRepository extends Repository
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Itec\Brand\Contracts\Brand';
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        Event::fire('catalog.brand.create.before');

        if (isset($data['locale']) && $data['locale'] == 'all') {
            $model = app()->make($this->model());

            foreach (core()->getAllLocales() as $locale) {
                foreach ($model->translatedAttributes as $attribute) {
                    if (isset($data[$attribute])) {
                        $data[$locale->code][$attribute] = $data[$attribute];
                    }
                }
            }
        }

        $brand = $this->model->create($data);

        $this->uploadImages($data, $brand);

        Event::fire('catalog.brand.create.after', $brand);

        return $brand;
    }

    /**
     * Specify brand tree
     *
     * @param integer $id
     * @return mixed
     */
    public function getBrandTree($id = null)
    {
        return $id
            ? $this->model::orderBy('id', 'ASC')->where('id', '!=', $id)->get()->toTree()
            : $this->model::orderBy('id', 'ASC')->get()->toTree();
    }


    /**
     * Get root categories
     *
     * @return mixed
     */
    public function getRootBrands()
    {
        return $this->model::withDepth()->having('depth', '=', 0)->get();
    }

    /**
     * get visible category tree
     *
     * @param integer $id
     * @return mixed
     */
    public function getVisibleBrandTree($id = null)
    {
        static $brands = [];

        if(array_key_exists($id, $brands))
            return $brands[$id];

        return $brands[$id] = $id
                ? $this->model::orderBy('id', 'ASC')->where('status', 1)->descendantsOf($id)->toTree()
                : $this->model::orderBy('id', 'ASC')->where('status', 1)->get()->toTree();
    }

    /**
     * Checks slug is unique or not based on locale
     *
     * @param integer $id
     * @param string  $slug
     * @return boolean
     */
    // public function isSlugUnique($id, $slug)
    // {
    //     $exists = CategoryTranslation::where('category_id', '<>', $id)
    //         ->where('slug', $slug)
    //         ->limit(1)
    //         ->select(\DB::raw(1))
    //         ->exists();

    //     return $exists ? false : true;
    // }

    /**
     * Retrive brand from slug
     *
     * @param string $slug
     * @return mixed
     */
    public function findBySlugOrFail($slug)
    {
        $brand = $this->model->whereTranslation('slug', $slug)->first();

        if ($brand) {
            return $brand;
        }

        throw (new ModelNotFoundException)->setModel(
            get_class($this->model), $slug
        );
    }

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute = "id")
    {
        $brand = $this->find($id);

        Event::fire('catalog.brand.update.before', $id);

        $brand->update($data);

        $this->uploadImages($data, $brand);

        Event::fire('catalog.brand.update.after', $id);

        return $brand;
    }

    /**
     * @param $id
     * @return void
     */
    public function delete($id)
    {
        Event::fire('catalog.brand.delete.before', $id);

        parent::delete($id);

        Event::fire('catalog.brand.delete.after', $id);
    }

    /**
     * @param array $data
     * @param mixed $brand
     * @return void
     */
    public function uploadImages($data, $brand, $type = "image")
    {
        if (isset($data[$type])) {
            $request = request();

            foreach ($data[$type] as $imageId => $image) {
                $file = $type . '.' . $imageId;
                $dir = 'brand/' . $brand->id;

                if ($request->hasFile($file)) {
                    if ($brand->{$type}) {
                        Storage::delete($brand->{$type});
                    }

                    $brand->{$type} = $request->file($file)->store($dir);
                    $brand->save();
                }
            }
        } else {
            if ($brand->{$type}) {
                Storage::delete($brand->{$type});
            }

            $brand->{$type} = null;
            $brand->save();
        }
    }

    public function getPartial()
    {
        $categories = $this->model->all();
        $trimmed = array();

        foreach ($categories as $key => $brand) {
            if ($brand->name != null || $category->name != "") {
                $trimmed[$key] = [
                    'id' => $brand->id,
                    'name' => $brand->name
                ];
            }
        }

        return $trimmed;
    }
}