@extends('brand_view::layouts.master')

@section('page_title')
    {{ __('brand_lang::app.brand.title') }}
@stop

@section('content')
    <div class="content">

        <form method="POST" action="{{ route('brand.store') }}" @submit.prevent="onSubmit" enctype="multipart/form-data">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/brand/dashboard') }}';"></i>

    					{{ __('brand_lang::app.brand.add-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
    					{{ __('brand_lang::app.brand.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()

                    <!-- <input type="text" hidden name="is_verified" value="1"> -->

                    <accordian :title="'{{ __('brand_lang::app.brand.general') }}'" :active="true">
                        <div slot="body">
                            <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                                <label for="name" class="required">{{ __('brand_lang::app.brand.name') }}</label>
                                <input type="text" v-validate="'required'" class="control" id="name" name="name" data-vv-as="&quot;{{ __('brand_lang::app.brand.name') }}&quot;"/>
                                <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                            </div>





                            <div class="control-group" :class="[errors.has('slug') ? 'has-error' : '']">
                                <label for="slug" class="required">{{ __('brand_lang::app.brand.slug') }}</label>
                                <input type="text" v-validate="'required'" class="control" id="slug" name="slug" value="{{ old('slug') }}" data-vv-as="&quot;{{ __('brand_lang::app.brand.slug') }}&quot;" v-slugify/>
                                <span class="control-error" v-if="errors.has('slug')">@{{ errors.first('slug') }}</span>
                            </div>




                            <description></description>




                            <div class="control-group" :class="[errors.has('image') ? 'has-error' : '']">
                                <label>{{ __('brand_lang::app.brand.image') }}

                                <image-wrapper :button-label="'{{ __('brand_lang::app.brand.add-image-btn-title') }}'" input-name="image" :multiple="false"></image-wrapper>

                                <span class="control-error" v-if="errors.has('image')">@{{ errors.first('image') }}</span>

                            </div>





                            <div class="control-group" :class="[errors.has('status') ? 'has-error' : '']">
                                <label for="status" class="required">{{ __('brand_lang::app.brand.status') }}</label>
                                <select class="control" v-validate="'required'" id="status" name="status" data-vv-as="&quot;{{ __('brand_lang::app.brand.status') }}&quot;">
                                    <option value="1">
                                        {{ __('brand_lang::app.brand.status-yes') }}
                                    </option>
                                    <option value="0">
                                        {{ __('brand_lang::app.brand.status-no') }}
                                    </option>
                                </select>
                                <span class="control-error" v-if="errors.has('status')">@{{ errors.first('status') }}</span>
                            </div>
                        </div>
                    </accordian>


                </div>
            </div>

        </form>
    </div>
@stop



@push('scripts')
    <script src="{{ asset('vendor/webkul/admin/assets/js/tinyMCE/tinymce.min.js') }}"></script>

    <script type="text/x-template" id="description-template">

<div class="control-group" :class="[errors.has('description') ? 'has-error' : '']">
    <label for="description" :class="isRequired ? 'required' : ''">{{ __('brand_lang::app.brand.description') }}</label>
    <textarea v-validate="isRequired ? 'required' : ''"  class="control" id="description" name="description" data-vv-as="&quot;{{ __('brand_lang::app.brand.description') }}&quot;">{{ old('description') }}</textarea>
    <span class="control-error" v-if="errors.has('description')">@{{ errors.first('description') }}</span>
</div>

    </script>

    <script>
        $(document).ready(function () {
            tinymce.init({
                selector: 'textarea#description',
                height: 200,
                width: "100%",
                plugins: 'image imagetools media wordcount save fullscreen code',
                toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent  | removeformat | code',
                image_advtab: true
            });
        });

        Vue.component('description', {

            template: '#description-template',

            inject: ['$validator'],

            data: function() {
                return {
                    isRequired: true,
                }
            },

            created: function () {
                var this_this = this;

                $(document).ready(function () {
                    $('#display_mode').on('change', function (e) {
                        if ($('#display_mode').val() != 'products_only') {
                            this_this.isRequired = true;
                        } else {
                            this_this.isRequired = false;
                        }
                    })
                });
            }
        })
    </script>
@endpush