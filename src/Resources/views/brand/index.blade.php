@extends('brand_view::layouts.master')

@section('page_title')
    {{ __('brand_lang::app.brand.title') }}
@stop

@section('content')
    <div class="content" style="height: 100%;">
        <div class="page-header">
            <div class="page-title">
                <h1>{{ __('brand_lang::app.brand.title') }}</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('brand.create') }}" class="btn btn-lg btn-primary">
                    {{ __('brand_lang::app.brand.add-type-btn-title') }}
                </a>
            </div>
        </div>

        {!! view_render_event('bagisto.brand.brand.list.before') !!}

        <div class="page-content">
            @inject('brand', 'Itec\Brand\DataGrids\BrandDataGrid')
            {!! $brand->render() !!}
        </div>

        {!! view_render_event('bagisto.brand.brand.list.after') !!}

    </div>
@stop